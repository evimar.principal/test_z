# Zmags Java Assigment

This packages solve 2 tasks of the zmags java assigment.  
**Triangle** task determinates the type of a triangle given the lengths of its sides.  
**Document** task says the most common words in a document.


## Getting Started
Each package includes the source files and its test suite as well.  
In the bin folder, there are {+*.class+} files organized into each corresponging packages.


### Packages Included:
1. triangle
2. document

### Test Unit:
 * **JUnit 5** Please read the documentation included at [doc](doc/) folder


## Triangle Package
The [class triangle](src/triangle) was created to determinate the type of a triangle,  
given the lenghts of its three 3 sizes. 

### Content
In the [src folder](src/triangle), there is a triangle folder with follows:

{+Triangle.java+}       is the main source of the class  
{+TriangleTest.class+}  is the suite case source for Triangle class 

### How to use
Use from a command line:
```command
    java -classpath bin triangle.Triangle  int_1 int_2 int_3
```

where {+int_1+}  {+int_2+} and {+int_3+} are the arguments, you must pass 3 integers greater than zero separated by spaces.
   
### Use from an IDE:

If you want to run the code in an IDE, such as Eclipse, you should
be able to copy the entire contents of `src/triangle` folders
into a project in the IDE, and then run the programs.

The core method of Triangle is **typeFromSides**, 
which accepts 3 parameters of type int greater then zero, and return one number of type byte. 
This is a class method.  
To invoke it write 
```java
Triangle.sideFromSides(1,1,1)
```
**Example of use:**
```java
  int side1 = 1, side2=2, side3= 3; 
  byte result = Triangle.sideFromSides(side1, side2, side3);  
```

The class Triangle also provide 4 constants for readable use of type of triangle returned.  
These are:
```
     Triangle.SCALENE, Triangle.ISOSCELES, Triangle.EQUILATERAL, Triangle.ERROR
```
You can compare the returned value, with any of them:
```java
if (result==Triangle.SCALENE) { do_something; }
```

## Document Package
The class document analyze a text file,  
and return the **top 10** common words containing in the file,  
ordered from Most common to less common.

* UNICODE chars are welcome.
* LARGE files are handle efficiently.


### Dependencies:
 * [Apache Commons IO 2.6](https://commons.apache.org/proper/commons-io/download_io.cgi "Apache Commons IO 2.6")


### Features
* It is case **INSENSITIVE**. All common words are returned in lower case.
* All accentuaton is conserved, because this can distinct a word from another.
* The apostrophe char ' is considered part of a whole word. 
       For instance father's is a whole word.
* The underscore char _ is considered a separation char. 
       This mean THIS_WORD, are considered two words: this word

### Content
In the src folder, there is a document folder with follows:

`Document.java`      is the main source of the class 
`DocumentTest.java`  is the suite case for Document class 

In the root path of the project, you will find several txt files for test cases
     *.txt               are several text files to test Document

### How to use
   
#### Use from an IDE:

If you want to run the code in an IDE, such as Eclipse, you should
be able to copy the entire contents of src/document folder
into a project in the IDE, and then run the programs.

The core method of Document is **mostCommonWords**, 
which accepts 1 parameter `fileName` of type **String** which is the name of file to be analyzed.  
This is a class method must be invoked with:
```java
    Document.mostCommonWords(fileName)
```
**Example of use:**
```java
  String file ="my_file.txt"; 
  try {
      List<String> result = Document.mostCommonWords(file); 
  } throws (IOException e) {
	*** HANDLE EXCEPTION CODE ***
  }
```
As you see, {+mostCommonWords+} method throws {-IOException-} that must be handle.


### Issues:
 - Hand files from a different directory path.
 - Recognize the idiom.
 - Remove stop words.


Feel free :thumbsup: to email me any question about this class at evimar.principal@gmail.com


Java version 1.8  
Author Evimar Principal  
version 1.0  

