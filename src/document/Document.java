
package document;

import java.io.File;
import java.io.IOException;

import java.util.Map;

import java.util.List;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils ;
import org.apache.commons.io.LineIterator;



/**
 * Functionality related to text files.
 * 
 * @author Evimar Principal evimar.principal@gmail.com
 *
 */
public class Document {
	private static final Pattern 
	    UNICODE_WORD_PATTERN = Pattern.compile("[\\w']+", Pattern.UNICODE_CHARACTER_CLASS);
	
		
	/*
	 * Extract all whole words from the string line
	 * 
	 * @param line a string where will be extracted the words
	 * @return
	 *   a list within all the words in the line string 
	 */
	protected static List<String> extractWords(String line){
		
		List<String> words = new ArrayList<String>();
		
		
		Matcher m = Document.UNICODE_WORD_PATTERN.matcher(line.replace('_',' '));

		while ( m.find() ) {
		    words.add(line.substring(m.start(), m.end()));
		}
		
		return words;
	
	}
	


    
	/*
	 * Calculate the most common words in a list of words case INSENSITIVE
	 * 	
	 * @param words a list of words
	 * @return at most 10 common words in the list in lower case
	 */
	protected static List<String> mostCommonWordsInList(List<String> words) {
		final int WORDS_TO_OUTPUT = 10;	
		
		if (words.isEmpty() ) { return new ArrayList<>();}
		
		//Count the occurrences of each word
		Map<String, Long> countedWords = words
										.stream()
										.collect(Collectors
												.groupingBy(String::toLowerCase, Collectors.counting()));
		//Sorted by most common words first
		List<String> top = countedWords
						.entrySet()
						.stream()
						.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
						.map(Map.Entry::getKey)
						.collect(Collectors.toList()) ;
		
		
		int wordsSize = top.size(); 
		top = top.subList(0, (wordsSize > WORDS_TO_OUTPUT) ? WORDS_TO_OUTPUT : wordsSize);
		
	
		return top;
	}
	

	/**
	 * List the top 10 common words inside the text file.
	 * 
	 * - Handle UNICODE chars.
	 * - Accentuation is conserved, because it can distinct different words.
	 * - Underscore char _ is considered a separator char. 
	 *     For instance: "this_word" will be treated like two words (this  word).
	 * - Words with apostrophe char (') are treated as whole word, 
	 *   however other kind of apostrophes such `’ will be treated as separator
	 *     For instance:  "mc'gregor"  will be one whole word  (mc'gregor)
	 *                    "mc’gregor"  will be two words  (mc  gregor)
	 *     
	 * @param fileName - a text file to be analyzed. Must be a valid text file 
	 *               and must exists in the path.
	 * 
	 * @return a list of top 10 common words inside the document 
	 *         ordered by occurrences descending.
	 *         normalize to lower case and with its accentuation. 
	 *         If the document has less than 10 distinct words, 
	 *         it will return all words ordered by occurrences descending.
	 *         
	 * @throws IOException if the fileName doesn't exists or cannot be located
	 */
	public static final List<String> mostCommonWords(String fileName) throws IOException {
		
		List<String> words = new ArrayList<String>();
		
		if (fileName==null) { throw new IOException("Invalid null file name reference.");}
		File file = new File(fileName) ;

		
		LineIterator iterator = FileUtils.lineIterator(file, "UTF-8");
		try {
		    while (iterator.hasNext()) {
		        String line = iterator.nextLine();
		        
		        words.addAll(Document.extractWords(line));
		       
		    }
		} finally {
			iterator.close();
		}
		

		
		return mostCommonWordsInList(words) ;
	}

	/* 
	 *  Main method of Document
	 */
	public static void main(String[] args) {
		if (args.length!=1) {
			System.out.println("One argument fileName is expected.");
			return;
		}
		String fileName = args[0];
		try {
			List<String> top = Document.mostCommonWords(fileName);
			if (!top.isEmpty()) {
				System.out.println("The most common words of "+
									fileName + 
									((top.size()==1)?"is: ":"are: "));
				top.forEach(p->System.out.println(p));
			}
		}  catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
		
	}	
}
