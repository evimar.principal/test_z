package document;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.Arrays;
import java.io.IOException;

class DocumentTest {
	/*
	 * Testing strategy to method extractWords
	 * 
	 * Quantity of words: 0, 1, >1
	 * 
	 * Type of word: ASCII, with accents, with -, with ', with _, with numbers, numbers alone
	 * 
	 * Charset:  English, Dano-Norwegian, Latin, Russian, Japanese, Portuguese, Arabic
	 * 
	 * Trailing spaces: at begining, at middle, at end
	 * 
	 * String length: 0, >0
	 */
	
	final List<String> emptyList = new ArrayList<>();
	final String a1Word = "$Ñá1!?";
	final String numbers = "FooBar_Foo_911_Foo911";
	final String fooBar = "Foo Bar,Foo        -,- Foo";
	final String danish = "      wordÆ,wordØa,wordÅ12 ";
	final String apostrophe = "Hamlet's father's ghost    ", 
			portuguese="doação", deutche="läßt,", 
			japan="或日の暮方の事である。一人の下人が、羅生門の下で雨やみを待っていた。",
			russian="МОСКОВІЯ  ПРЕДСТАВЛЕНІИ ИНОСТРАНЦЕВЪ",
			arabic="ورحل مايكل هارت.. قدّيس الكتاب الرقمي!";
	
	final List<String> list10 = new ArrayList<>(Arrays.asList("1","2","3","4","5",
            "6","7","8","9","10"));

	
	//covers quantity of words 0, string len 0
	@Test
	void testExtractWordsEmptyString() {
	
		assertIterableEquals(emptyList, Document.extractWords(""));
		
		
	}
	
	//covers Latin accentuation word, qty 1, special chars between
	@Test
	void testExtractWordsOnlyOneBetweenSpecialChars() {
		List<String> a1Clean = new ArrayList<String>(Arrays.asList("Ñá1"));
		
		assertIterableEquals(a1Clean, Document.extractWords(a1Word));
	}
	
	//covers several words without space only special chars
	@Test
	void testExtractWordsRepeatedBetweenSpecialChars() {
		List<String> a1Clean = new ArrayList<String>(Arrays.asList("Ñá1","Ñá1","Ñá1"));
	
		assertIterableEquals(a1Clean, Document.extractWords(a1Word+a1Word+a1Word));
	}
	
	// covers chars _ and word with numbers, numbers alone
	@Test
	void testExtractWordsWithNumbers() {
		List<String> a1Clean = new ArrayList<String>(
				Arrays.asList("FooBar","Foo","911","Foo911"));
		
		assertIterableEquals(a1Clean, Document.extractWords(numbers));
	}
	
	// covers trailing spaces in the middle, token with only special chars
	@Test
	void testExtractWordsWithWithSeparator() {
		List<String> a1Clean = new ArrayList<String>(
				Arrays.asList("Foo","Bar","Foo","Foo"));

		
		assertIterableEquals(a1Clean, Document.extractWords(fooBar));
	}
	
	//Danish alphabet char, trailing spaces at begining
	//Portuguese, deutche, japanese, russian, arabic
	@Test
	void testExtractWordsInOtherIdioms() {
		List<String> clean = new ArrayList<String>(
				Arrays.asList("wordÆ","wordØa","wordÅ12"));

		
		assertIterableEquals(clean, Document.extractWords(danish));
		
		clean = new ArrayList<String>(
				Arrays.asList(portuguese));
		
		assertIterableEquals(clean, Document.extractWords(portuguese));
		
		clean = new ArrayList<String>(
				Arrays.asList("läßt"));
		
		assertIterableEquals(clean, Document.extractWords(deutche));
		
		clean = new ArrayList<String>(
				Arrays.asList("或日の暮方の事である","一人の下人が","羅生門の下で雨やみを待っていた"));
		
		assertIterableEquals(clean, Document.extractWords(japan));

		clean = new ArrayList<String>(
				Arrays.asList("МОСКОВІЯ","ПРЕДСТАВЛЕНІИ","ИНОСТРАНЦЕВЪ"));
		
		assertIterableEquals(clean, Document.extractWords(russian));


		clean = new ArrayList<String>(
				Arrays.asList("ورحل","مايكل","هارت","قدّيس","الكتاب","الرقمي"));
	
		assertIterableEquals(clean, Document.extractWords(arabic));


	}

	//covers conserve words with apostrophe, trailing spaces at end
	@Test
	void testExtractWordsApostropheChars() {
		List<String> clean = new ArrayList<String>(
				Arrays.asList("Hamlet's","father's","ghost"));
		
		assertIterableEquals(clean, Document.extractWords(apostrophe));
	}
	
	/*
	 * Testing strategy of method MostCommonWordsInList
	 * 
	 *  Size of list: 0, 1, 10, >10
	 *  Occurrences of the top words: same, different
	 *  Case sensitive: lower, UPPER, rAnDom
	 *  Accentuation: same chars with different accentuation
	 */
	//covers size of list 0
	@Test
	void testMostCommonWordsInListEmpty() {
		List<String> list = new ArrayList<>();
		
		assertIterableEquals(list, Document.mostCommonWordsInList(
											Document.extractWords("")));
	}
    
	//covers one word 1 occurrence, and repeated, UPPER and lower case
	@Test
	void testMostCommonWordsInListOneWord() {
		List<String> list = new ArrayList<>();
		
		list.add("aa");
		assertIterableEquals(list, Document.mostCommonWordsInList(
				Document.extractWords("aa")));
		assertIterableEquals(list, Document.mostCommonWordsInList(
				Document.extractWords("aa AA aa aa")));
	}
	
	//covers more than 10 words, same quantity of occurrences on top words,
	//differentiation with accented words
	@Test
	void testMostCommonWordsInListSeveralWords() {
		List<String> list = Document.mostCommonWordsInList(
				Document.extractWords("1 2 3 4 5 6 7 8  á a " +
									  "1 2 3 4 5 6 7 8 9 1 2 "+
									  "3 4 11 12 13 14 unica á a 8 8 "));
		
		assertEquals(10, list.size());
		assertTrue(list.contains("1"));
		assertTrue(list.contains("2"));
		assertTrue(list.contains("3"));
		assertTrue(list.contains("4"));
		assertTrue(list.contains("5"));
		assertTrue(list.contains("6"));
		assertTrue(list.contains("7"));
		assertTrue(list.contains("8"));
		assertTrue(list.contains("a"));
		assertTrue(list.contains("á"));
		assertEquals("8",list.get(0));
	}

	//covers different occurrence on top words, more than 10 words
	@Test
	void testMostCommonWordsInListOrders() {
		List<String> list2 = Document.mostCommonWordsInList(
				Document.extractWords("1 2 3 4 5 6 7 8 9 10 " + 
									  "1 2 3 4 5 6 7 8 9 10 " +
									  "1 2 3 4 5 6 7 8  "+
									  "1 2 3 4 5 6 7  "+
									  "1 2 3 4 5 6   "+
									  "1 2 3 4 5  "+
									  "1 2 3 4  "+
									  "1 2 3 "+
									  "1 2  "+
									  "1  "+
									  "this words only one time appear "));

		
		assertIterableEquals(list10, list2);
	}

	//covers different occurrence on top, less than 10 top words, rAmDom case
	@Test
	void testMostCommonWordsInListLessThanTen() {
		List<String> list = new ArrayList<>(
				Arrays.asList("aa","bb","ccc","cc"));

		assertIterableEquals(list, Document.mostCommonWordsInList(
				Document.extractWords("ccc aa aa aa aa bb aa bb bb CcC cc")));
	}


	//covers different occurrence on top, less than 10 top words, accentuation agregate
	@Test
	void testMostCommonWordsInListAccentuation() {
		List<String> list = new ArrayList<>(
				Arrays.asList("aá","aa","áá"));

		assertIterableEquals(list, Document.mostCommonWordsInList(
				Document.extractWords("aá aá aá aa aa áá")));
	}

	/*
	 * Testing strategy of method MostCommonWords
	 *  File name: null reference, does not exists
	 *  File content: Nonchars, without words, single-ton, same repeated, several
	 *  Occurrences of the top words: same, different
	 *  Case sensitive: lower, UPPER, rAnDom
	 *  Accentuation: same chars with different accentuation
	 */

	//covers file content Nonchars
	@Test
	void testMostCommonWordsEmptyFile() throws IOException {

		List<String> list = new ArrayList<>();
		String file = "empty.txt";
		assertIterableEquals(list, Document.mostCommonWords(file));

	}

	//covers file null reference
	@Test
	void testMostCommonWordsNullFileReference() throws IOException {

		String file = null ;
		assertThrows(IOException.class, 
				()->{Document.mostCommonWords(file);} );

		}	

	//covers file doesn't exists
	@Test
	void testMostCommonWordsNoSuchFile() throws IOException {

		String file = "no_exists" ;
		assertThrows(IOException.class, 
				()->{Document.mostCommonWords(file);} );

		}	

	//covers file without words
	@Test
	void testMostCommonWordsWitoutWords() throws IOException {

		List<String> list = new ArrayList<>();
		String file = "nowords.txt";
		assertIterableEquals(list, Document.mostCommonWords(file));

	}

	//covers sigleton, one top word
	@Test
	void testMostCommonWordsSingleton() throws IOException {

		List<String> list = new ArrayList<>();
		list.add("word");
		String file = "singleton.txt";
		assertIterableEquals(list, Document.mostCommonWords(file));


	}

	//covers one word, same word repeated with Case Insensitive UPPER, lower, RaNdOm
	@Test
	void testMostCommonWordsOneWord() throws IOException {

		List<String> list = new ArrayList<>();
		list.add("word");

		String file2 = "repeated.txt";
		assertIterableEquals(list, Document.mostCommonWords(file2));

	}

	//cover 10 top words ordered
	@Test
	void testMostCommonWordsOrdered() throws IOException {

		String file = "ten_ordered.txt";

		assertIterableEquals(list10, Document.mostCommonWords(file));

	}

	//cover less than 10 words, _ char, accentuation aggregation
	@Test
	void testMostCommonWordsAccentuation() throws IOException {

		List<String> list = new ArrayList<>(Arrays.asList("árco","arco","arcó"));

		String file = "accent.txt";

		assertIterableEquals(list, Document.mostCommonWords(file));

	}

	//covers arabic chars
	@Test
	void testMostCommonWordsArabic() throws IOException {

		List<String> list = new ArrayList<>(Arrays.asList("التنسيق","مشروع","لتحقيق"
													,"بنفسه","المحلية","البسيطة"
													,"مباهج","مواقع","التي","الشاب"));

		String file = "arabic.txt";

		assertIterableEquals(list, Document.mostCommonWords(file));
	
	}

	//covers Danish chars
	@Test
	void testMostCommonWordsDanish() throws IOException {

		List<String> list = new ArrayList<>(Arrays.asList("øjet","næsten","trækkes"
													,"sprælle","hældning","vingefjær"
													,"møllebakken","bevægelser","grævlingekøter","zmags"));

		String file = "danish.txt";

		assertIterableEquals(list, Document.mostCommonWords(file));
	
	}

	//covers japanese chars
	@Test
	void testMostCommonWordsJapanese() throws IOException {

		List<String> list = new ArrayList<>(Arrays.asList("狐狸","金銀","勇気",
								  "羅生門","昼間見","二三人",
								  "芥川龍之介","けであかくなる","もそうしなければ","とうとうしまいには"));

		String file = "japanese.txt";

		assertIterableEquals(list, Document.mostCommonWords(file));
	
	}

	//covers russian chars
	@Test
	void testMostCommonWordsRussian() throws IOException {

		List<String> list = new ArrayList<>(Arrays.asList("Гдѣ","буквы","ОЧЕРКИ",
								  "поѣдаютъ","АПОСТОЛА","ЛУКОМСКІЙ",
								  "кончалась","МОСКОВІЯ","ПРЕДСТАВЛЕНІИ","ИНОСТРАНЦЕВЪ"));
		
		String file = "russian.txt";
		list=list.stream().map(String::toLowerCase).collect(Collectors.toList());
		assertIterableEquals(list, Document.mostCommonWords(file));
	
	}



}
