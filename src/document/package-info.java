/**
 * Collect classes to manage Document.
 * Mainly to solve the task document, in the Java Assignment at zmags.
 * @author Evimar Principal evimar.principal@gmail.com
 */
package document;