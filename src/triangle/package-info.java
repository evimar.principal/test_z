/**
 * 
 *  Collect Classes related to triangle shape.
 *  Mainly to solve the task triangle at zMags Java Assignment.
 * @author Evimar Principal evimar.principal@gmail.com
 */
package triangle;