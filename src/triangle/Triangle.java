
package triangle;

/**
 * 
 * Collect functionality related to the shape Triangle
 * 
 * 
 *
 */
public class Triangle {

   public static final int 
			SCALENE    = 1, 
			ISOSCELES  = 2,
			EQUILATERAL= 3,
			ERROR      = 4;
   /**
    * Determine the type of a triangle given the length of its sides.
    * Must pass three lengths the sides of the triangle in ANY order
    * and must be valid lengths integer positive and greater than 0 
    *  
    * 
    * @param sideLen1 the length of a side 1 must be integer positive
    * @param sideLen2 the length of a side 2 must be integer positive
    * @param sideLen3 the length of a side 3 must be integer positive
    * 
    * @return Integer, describing the triangle type: 1=scalene, 2=isosceles, 3=equilateral, 4=error
    */	
   public static byte typeFromSides(int sideLen1, int sideLen2, int sideLen3) {
	   
	
	    //None zero length 
		if (sideLen1<=0 || sideLen2<=0 || sideLen3<= 0) {
			throw new RuntimeException("All lengths must be valid lenghts greater than zero.");
		}
		
		// Equilateral Test
		if (sideLen1==sideLen2 && sideLen2==sideLen3) { 
			return Triangle.EQUILATERAL ;
		}
		
		//Is it a valid triangle?
		long majorSide = (long) Math.max(sideLen1, (long)Math.max(sideLen2, sideLen3));
		long otherSides = (long)sideLen1 + (long)sideLen2 + (long)sideLen3 - majorSide;
		
		if (majorSide >= otherSides) {
			return Triangle.ERROR;
		}
		
		//SCALENE all sides differents
		if (sideLen1!=sideLen2 && sideLen2!= sideLen3 && sideLen1!=sideLen3) {
		return Triangle.SCALENE;
		} else {
			return Triangle.ISOSCELES;  //all previous test failed, It is equilateral
			}
		    	    
	}
	
   private static boolean isInteger(String str) {
	    if (str == null) {
	        return false;
	    }
	    int length = str.length();
	    if (length == 0) {
	        return false;
	    }
	    int i = 0;
	    if (str.charAt(0) == '-') {
	        if (length == 1) {
	            return false;
	        }
	        i = 1;
	    }
	    for (; i < length; i++) {
	        char c = str.charAt(i);
	        if (c < '0' || c > '9') {
	            return false;
	        }
	    }
	    return true;
	}
   

   /**
    * Main function of program.
    * @param args command-line arguments
    */
   public static void main(String[] args) {
	   
	   if (args.length!=3) {
		   	System.out.println("3 integer arguments expected.");
		   	return ;
	   }
	   
	   for (String variable: args){
		   if (!Triangle.isInteger(variable)) {
			   	System.out.println("Invalid argument "+variable+" must be an integer.");
			   	return;
		   }
	   }
       System.out.println("For the triangle of lenght sides: "+args[0]+" - "+args[1]+" - "+args[2]);
       byte result = typeFromSides(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]));
       System.out.print("The type of triangle is: ");
       System.out.print(result);
       String strType =" ";
       switch (result) {
	       case Triangle.EQUILATERAL: {strType += "Equilateral"; break; }
	       case Triangle.ISOSCELES  : {strType += "Isosceles";   break; }
	       case Triangle.ERROR      : {strType += "Error";       break; }
	       case Triangle.SCALENE    : {strType += "Scalene"; 	break; }
	   }
       System.out.println(strType);
       
       
       
   }


}