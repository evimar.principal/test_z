package triangle;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TriangleTest {
	/*
     * Testing strategy:
     * 
     * Lenght of the side:
     *    side Minimum==1  side in the middle ==10  side Maximun== MAXIM
     *    
     * Order of the side
     *    Ascending, descending, disorder
     *    
     * Type of triangle
     *     Isosceles, Scalene, Equilateral or error
     *      
     * Each of these parts is covered by at least one test case below.
     */
	
	final int sideMinimum=1, sideMiddle = 10, sideMax = Integer.MAX_VALUE;
	
    
    // covers Length minimum, middle and maximum, Order Ascending, Type Error
    @Test
    public void testTriangleErrorAscending() {
        assertEquals(Triangle.ERROR, Triangle.typeFromSides(sideMinimum, sideMiddle, sideMax));
    }

    // covers Length minimum, middle and maximum, Order Ascending, Type Error
    @Test
    public void testTriangleErrorTwoSidesEqualsAscending() {
        assertEquals(Triangle.ERROR, Triangle.typeFromSides(sideMinimum, sideMinimum, sideMax));
    }

    // covers Length minimum, middle and maximum, Order Descending, Type Error
    @Test
    public void testTriangleErrorTwoSidesEqualsDescending() {
        assertEquals(Triangle.ERROR, Triangle.typeFromSides(sideMax, sideMinimum, sideMinimum));
    }

    // covers Length minimum, middle and maximum, Order Disorder, Type Error
    @Test
    public void testTriangleErrorTwoSidesEqualsDisorder() {
        assertEquals(Triangle.ERROR, Triangle.typeFromSides(sideMinimum, sideMax, sideMinimum));
    }

    // covers Length  maximum,  Type Isosceles
    @Test
    public void testTriangleEquilateralHuge() {
        assertEquals(Triangle.EQUILATERAL, Triangle.typeFromSides(sideMax, sideMax, sideMax));
    }

    // covers Length  minimum,  Type Isosceles
    @Test
    public void testTriangleEquilateralTiny() {
       assertEquals(Triangle.EQUILATERAL, Triangle.typeFromSides(sideMinimum, sideMinimum, sideMinimum));
    }
    
    // covers Length  middle, Order Descending, Type Scalene
    @Test
    public void testTriangleScaleneDescending() {
        assertEquals(Triangle.SCALENE, Triangle.typeFromSides(sideMiddle*4, sideMiddle*3, sideMiddle*2));
    }
    
    // covers Length  max, Order Descending, Type Scalene
    @Test
    public void testTriangleScaleneAscendingHuge() {
        assertEquals(Triangle.SCALENE, Triangle.typeFromSides(sideMax-100, sideMax-50, sideMax));
    }
    
    // covers Length  middle, Order disorder, Type Scalene
    @Test
    public void testTriangleScaleneDisorder() {
        assertEquals(Triangle.SCALENE, Triangle.typeFromSides(sideMiddle+5,sideMiddle+10,sideMiddle));
    }
    
    // covers Length  min and middle, Order disorder, Type Equilateral
    @Test
    public void testTriangleIsoscelesDisorder() {
        assertEquals(Triangle.ISOSCELES, Triangle.typeFromSides(sideMiddle, sideMinimum, sideMiddle));
    }
    
    // covers Length  min and max, Order ascending, Type Equilateral
    @Test
    public void testTriangleIsoscelesAscending() {
        assertEquals(Triangle.ISOSCELES, Triangle.typeFromSides(sideMinimum, sideMax, sideMax));
    }
    
    // covers Length  middle and max, Order descending, Type Equilateral
    @Test
    public void testTriangleIsoscelesDescending() {
        assertEquals(Triangle.ISOSCELES, Triangle.typeFromSides(sideMax, sideMax, sideMiddle));
    }
    
}
